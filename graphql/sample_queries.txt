{
  profile(search:{id: 5}) {
    id
    nickname
    name
    summary
    icon
    types
    primaryUser {
      id
      email
      password
    }
    jsonData
    followingCount
    followersCount
    following {
      id
      nickname
      uri
      icon
    }
    followers {
      id
      nickname
      uri
      icon
    }
    memberships {
      id
      target {
        id
        uri
        nickname
      }
    } 
    otherRelationships {
      relationship
      subject {
        id
        nickname
        uri
      }
      target {
        id
        nickname
        uri
      }
    }
  }
  user(search:{id: 5}) {
    id
    email
    password
    primaryProfile {
      id
      nickname
    }
  }
  group(search:{id: 5}) {
    openness
    types
    profile {
      uri
      local
    }
    members {
      subject {
        nickname
        uri
        icon
      }
      relationship
    }
  }
  activities (by_profile: {id: 42}) {
    id
    uri
    types
    local
    published
    fromProfileURI
    objectURI
    fromProfile {
      id
      name
      uri
      icon
    }
    object {
      id
      uri
      types
      shareCount
      likeCount
      jsonData
    }
    jsonData
  }
}
