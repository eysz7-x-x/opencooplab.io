CommonsPub is a back-end server written in Elixir (running on the Erlang VM, and using the Phoenix web framework) to be highly performant and can run on low powered devices like a Raspberry Pi. Each app will likely have a bespoke front-end (though they're of course encouraged to share code).

Users should be able to: use any number of apps as modules on top of CommonsPub, participate in a federated ecosystem, rely on a straightforward user experience, and for the first time, use the same decentralised identity for all their apps.

Developers should find it much easier to write federated apps by focusing only on the application-specific vocabulary and logic, whilst federation, data schemas and identity will be available out of the box thanks to CommonsPub.

Read more about [the purpose of this project](/page/about/), and [what apps are using it?](/page/apps/)