---
title: Why CommonsPub
subtitle: Why this project exists
comments: false
---

We want to facilitate the process of creating an open source federated app ecosystem. This will be achieved by creating an unopinionated server federated through the ActivityPub w3c standard.

ActivityPub gives applications a shared vocabulary that they can use to communicate with each other. If a server implements ActivityPub, it can publish posts that any other server that implements ActivityPub knows how to share, like and reply to. 
But the vocabulary can be extended to represent other types of activities beyond the initial social ones, including economic and political activities.

So far writing software that implements the logic of federation is technically challenging, even when attempting to implement a very simple app.
CommonsPub gives developers the ability to focus solely on the specific logic of their application by providing a backend that manages the database and federation out of the box.

CommonsPub enables anyone to deploy a server and federate with other instances on the web that share the same protocols, like Mastodon, Pleroma, GNU Social, Diaspora, Peertube, etc - synchronising any kind of data and activities in a p2p environment, without any central authority.

CommonsPub is also a way to promote diversity in decentralization by allowing developers and groups to create apps tailored to their specific needs, or to fork existing apps in order to change their social, political and economical features and algorithms.