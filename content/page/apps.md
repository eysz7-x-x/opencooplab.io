---
title: Apps powered by CommonsPub
subtitle: Implementations, plugins & modules, client apps, etc
comments: false
---

The first projects using and contributing to CommonsPub are:

* [MoodleNet](https://moodle.com/moodlenet) to empower communities of educators to connect, learn, and curate open content together

* [Open Cooperative Ecosystem](https://opencoopecosystem.net/) to empower economic activities driven by human and ecological needs rather than profit
